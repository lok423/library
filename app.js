const express = require('express');
const chalk = require('chalk');
const debug = require('debug')('app');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;
const nav = [{link: '/books',title:'Books'}, {link:'/authors',title:'Authors'}]
const bookRouter = require('./src/routes/bookRoutes')(nav);
const adminRouter = require('./src/routes/adminRoutes')(nav);
const authRouter = require('./src/routes/authRoutes')(nav);
const dashboardRouter = require('./src/routes/dashboardRoutes')(nav);
const uploadRouter = require('./src/routes/uploadRoutes')();
//const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const sql = require('mssql/msnodesqlv8');
const config = {
    server: 'DESKTOP-IF0IO4R', // You can use 'localhost\\instance' to connect to named instance
    database: 'PSLibrary',
    driver: "msnodesqlv8",
    port : 1433,

    options: {
        encrypt: false, // Use this if you're on Windows Azure
        trustedConnection:true
    },
    pool: {
        acquireTimeoutMillis: 15000
    }
};
sql.connect(config).catch((err) => debug(err));

app.set('views', './src/views');
//app.set('view engine', 'pug');
app.set('view engine', 'ejs');

app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(cookieParser());
app.use(session({secret: 'library'}));

require('./src/config/passport.js')(app);

app.use(express.static(path.join(__dirname, '/public/')));
app.use('/css', express.static(path.join(__dirname, '/node_modules/bootstrap/dist/css')));
app.use('/js', express.static(path.join(__dirname, '/node_modules/bootstrap/dist/js')));
app.use('/js', express.static(path.join(__dirname, '/node_modules/jquery/dist')));
app.use('/scripts', express.static(path.join(__dirname, '/src/scripts')));
app.use('/styles', express.static(path.join(__dirname, '/src/styles')));

app.use('/books', bookRouter);
app.use('/admin', adminRouter);
app.use('/auth', authRouter);
app.use('/dashboard', dashboardRouter);
app.use('/upload', uploadRouter )



app.get('/', function (req, res) {
    //res.sendfile(path.join(__dirname ,'/views/', '/index.html'));
    res.render('index', {
        title: 'MyLibrary',
        nav
    });
});

app.listen(port, function () {
    debug(`listening on port ${chalk.green(port)} `);
});