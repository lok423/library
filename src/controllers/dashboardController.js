const {
  MongoClient
} = require('mongodb');
const debug = require('debug')('app:dashboardController');


function dashboardController(nav){
  function getIndex(req, res)  {


      const url = 'mongodb://localhost:27017';
      const dbName = 'finance';

      (async function mongo() {
              let client;
              try {
                  client = await MongoClient.connect(url);

                  debug('Connected correctly to server');

                  const db = client.db(dbName);

                  const col = await db.collection('finances');

                  const bnuLatestTransaction = await col.findOne({bank:'BNU'},{sort: {transactionDate: -1}});
                  const bnuPastMonthLatestTransaction = await col.findOne({bank:'BNU',transactionDate: {$lte:new Date(new Date().setMonth(new Date().getMonth()-1))}},{sort: {transactionDate: -1}});

                  const bocLatestTransaction = await col.findOne({bank:'BOC'},{sort: {transactionDate: -1}});
                  const bocPastMonthLatestTransaction = await col.findOne({bank:'BOC',transactionDate: {$lte:new Date(new Date().setMonth(new Date().getMonth()-1))}},{sort: {transactionDate: -1}});

                  const currentMonth = await col.find({bank:'BNU',transactionDate: {$lt:new Date(),$gte: new Date(new Date().setMonth(new Date().getMonth()-1))}},{sort: {transactionDate: 1}}).toArray();
                  const pastMonth = await col.find({bank:'BNU',transactionDate: {$lt:new Date(new Date().setMonth(new Date().getMonth()-1)),$gte: new Date(new Date().setMonth(new Date().getMonth()-2))}},{sort: {transactionDate: 1}}).toArray();

                  debug(currentMonth);

                  res.render('dashboard', {
                      title: 'MyLibrary',
                      bnuLatestTransaction,
                      bocLatestTransaction,
                      bnuPastMonthLatestTransaction,
                      bocPastMonthLatestTransaction,
                      currentMonth,
                      pastMonth,
                      nav
                  });
              } catch (err) {
                  debug(err.stack);
            
              }
          client.close(); 

      }());


}
return {
  getIndex,
 
};
}

module.exports = dashboardController;