
const mongoose = require('mongoose');
const Finance = require('../models/bankStatementModel')
const fs = require('fs');
const excelToJson = require('convert-excel-to-json');
const formidable = require('formidable');
const util = require('util');
const debug = require('debug')('app:uploadController');


function uploadController() {
  function processUpload(req, res) {


    debug('uploadRouter');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {

      var oldpath = files.filetoupload.path;
      var newpath = 'contents\\' + files.filetoupload.name;
      fs.rename(oldpath, newpath, function (err) {
        if (err) throw err;
        res.writeHead(200, {
          'content-type': 'text/plain'
        });
        const result = excelToJson({
          source: fs.readFileSync(newpath) // fs.readFileSync return a Buffer
        });
        debug(result);
        res.write('received upload:\n\n');
        res.end(util.inspect({fields: fields, files: files}));
      });


    });






  }
  return {
    processUpload,

  };
}

module.exports = uploadController;