const mongoose = require('mongoose');
const {
  Schema,
} = mongoose;

const bankStatementModel = new Schema({
  bank:{
    type: String,
    default: 'BNU'
  },
  transactionType:{
type: String,
  },
  transactionDate: {
    type: Date,
  },
  processedDate: {
    type: Date,
  },
  description: {
    type: String,
  },
  withdrawal: {
    type: Number,
  },
  deposit: {
    type: Number,
  },
  balance: {
    type: Number,
  },
  actionDate: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model('Finance', bankStatementModel);