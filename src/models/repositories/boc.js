'use strict';
const fs = require('fs');
const BOCModel =require('./models/bocTransactionTypeModel');
const Finance =require('./models/bankStatementModel');
const excelToJson = require('convert-excel-to-json');
 const mongoose = require('mongoose');
const result = excelToJson({
    source: fs.readFileSync('./test/boc/DownloadAccountTransLog7.xls') // fs.readFileSync return a Buffer
});
var moment = require('moment');


const source = result['Sheet1'];
//console.log(source);


mongoose.connect('mongodb://localhost:27017/finance');
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

function formatNumber(num) {
  return Number(num.replace(',',''));
}

let array = [];
for(var i=3; i<source.length;i++){
  let _transactionDate = '';
  let _processedDate = '';
let _desc = '';
let _withdrawals = 0;
let _deposits = 0;
let _bal = 0;
let _type = '';
  if('A' in source[i]){
    _transactionDate = source[i].A;
    if('B' in source[i]){
      let key = source[i].B;
      if(key in BOCModel){
        //console.log(BOCModel[key]);
        _type = BOCModel[key];
      };
      
    
    }if('E' in source[i]){
      _withdrawals = source[i].E;
    }if('F'in source[i]){
      _deposits = source[i].F;
    }if('G'in source[i]){
      _bal = source[i].G;
    }
    if('H'in source[i]){
      _desc = source[i].H;
    }

    var record = {
      bank:'BOC',
      transactionDate:  moment(new Date(_transactionDate)).add(23,'hours').add(-i,'minutes'),
      //processedDate: _processedDate,
      description: _desc,
      withdrawal: formatNumber(_withdrawals).toFixed(2),
      deposit: formatNumber(_deposits).toFixed(2),
      balance: formatNumber(_bal).toFixed(2),
      transactionType: _type
    };

    const finance = new Finance(record);
  //console.log(finance);
    array.push(finance);
  }
  //console.log(array,array.length);
}
db.once('open', function(){
  console.log("ConnectionSuccessful!");
   // save multiple documents to the collection referenced by Book Model
   Finance.collection.insert(array, function (err, docs) {
    if (err){ 
        return console.error(err);
    } else {
      console.log("Multiple documents inserted to Collection");
    }
  });   
});

console.log(array,array.length);