
const Finance =require('./models/bankStatementModel')
 const mongoose = require('mongoose');
var moment = require('moment');
const debug = require('debug')('app:bnuProcess');

function bnuProcess(doc){
//console.log(result['Statment Sheet']);
mongoose.connect('mongodb://localhost:27017/finance');
const db = mongoose.connection;
db.on('error', debug('connection error:'));

let array = [];

const source = doc['Statment Sheet'];
for(var i=1; i<source.length;i++){
  let _transactionDate = '';
  let _processedDate = '';
let _desc = '';
let _withdrawals = 0;
let _deposits = 0;
let _bal = 0;
  if('A' in source[i]){
    _transactionDate = source[i].A;
    if('B' in source[i]){
      _processedDate = source[i].B;
    }if('C' in source[i]){
      _desc = source[i].C;
    }if('D' in source[i]){
      _withdrawals = source[i].D.toFixed(2);
    }if('E' in source[i]){
      _deposits = source[i].E.toFixed(2);
    }if('F'in source[i]){
      _bal = source[i].F.toFixed(2);
    }

    var record = {
      transactionDate:  moment(_transactionDate).add(23,'hours').add(-i,'minutes'),
      processedDate: _processedDate,
      description: _desc,
      withdrawal: _withdrawals,
      deposit: _deposits,
      balance: _bal
    };

    const finance = new Finance(record);
  //console.log(finance);
    array.push(finance);
  }

}
db.once('open', function(){
  debug("ConnectionSuccessful!");
   // save multiple documents to the collection referenced by Book Model
   Finance.collection.insert(array, function (err, docs) {
    if (err){ 
        return debug(err);
    } else {
      debug("Multiple documents inserted to Collection");
    }
  });   
});
debug(array);
}

module.exports = bnuProcess;