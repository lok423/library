const transactionType = {
  "Consumption": "POS",
 "Deposit":"ATM",
 "Withdrawal":"ATM",
 "Outward Transfer":"EBK",
 "Charging":"BKF",
 "International Outward Remittance":"EFT",
"Transfer":"APS",
"Interest Settlement": "IS",
"Inward Transfer": "EBK"
}

module.exports = transactionType;