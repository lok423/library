const express = require('express');
const bookRouter = express.Router();
const bookController = require('../controllers/bookController');
//const sql = require('mssql/msnodesqlv8');


//Use Sql
/*
function router(nav) {
    bookRouter.route('/')
        .get((req, res) => {

            (async function query() {
                const request = new sql.Request();

                const {recordset} = await request.query('select * from books');

                res.render('bookListView', {
                    title: 'MyLibrary',
                    books: recordset,
                    nav
                });

            }());


        });
    bookRouter.route('/:id')
    .all((req,res,next)=>{
        (async function query() {
            const {
                id
            } = req.params;
            const request = new sql.Request();
            const {recordset} = await request.input('id', sql.Int, id)
            .query('select * from books where id = @id');
            [req.book] = recordset;
            next();
        }());

    })
        .get((req, res) => {
          
            res.render('bookView', {
                title: 'MyLibrary',
                nav,
                book: req.book
            });

        });
    return bookRouter
}
*/

function router(nav) {
    const {getIndex,getById, middleware} = bookController(nav);
    bookRouter.use(middleware);
    bookRouter.route('/')
        .get(getIndex);
bookRouter.route('/:id')
    .get(getById)
    
return bookRouter
}

module.exports = router;