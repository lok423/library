const express = require('express');
const dashboardRouter = express.Router();
const dashboardController = require('../controllers/dashboardController');

function router(nav) {
  const {getIndex} = dashboardController(nav);

  dashboardRouter.route('/')
  .get(getIndex);
  return dashboardRouter;
}

module.exports = router;