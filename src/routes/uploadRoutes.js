const express = require('express');


const uploadRouter = express.Router();
const debug = require('debug')('app:uploadRoutes');

const uploadController = require('../controllers/uploadController');


function router() {
  const {processUpload} = uploadController();
  uploadRouter.route('/')
      .post(
        processUpload
      ).get((req,res)=>{
        debug("upload");
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write('<form action="upload" method="post" enctype="multipart/form-data">');
        res.write('<input type="file" name="filetoupload"><br>');
        res.write('<input type="submit">');
        res.write('</form>');
        return res.end();
      });





  return uploadRouter;

}

module.exports = router;